import axios from 'axios';

const api = axios.create({
  baseURL: 'http://centralmonitoramento.powercentral.com.br/api',
});


export default api;