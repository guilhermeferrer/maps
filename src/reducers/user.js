export default function user(state = { }, action) {
    switch (action.type) {
        case 'LOGIN': 
            return { ...state, token: action.token };
        case 'LOGOUT': 
            return { };
        default:
            return state;
    }
}