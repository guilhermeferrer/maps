import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as userActions from '../../actions/user';
import Touchable from 'react-native-platform-touchable';

import styles from './styles';

import api from '../../services';
import { ScrollView } from 'react-native-gesture-handler';

class CarList extends Component {

    state = {
        loading: true,
        vehicles: []
    }

    componentDidMount() {
        this.loadCars();
    }

    loadCars = async () => {
        this.setState({ error: "", loading: true });
        const formData = new FormData();

        formData.append('txtToken', this.props.token);

        await api({
            method: 'post',
            url: '/?method=getVehicles',
            data: formData
        }).then((response) => {
            console.log(response);
            if (response.data.msg === "Token expirado, cancelado ou não encontrado.") {
                return this.props.logout();
            }
            this.setState({ loading: false, vehicles: response.data.aData });
        }).catch((error) => {
            console.log(error.response);
        });
    }

    getLogo = (fabricante) => {
        switch (fabricante) {
            case 'Ford':
                return "http://logosvg.com/wp-content/uploads/2017/03/Ford_logo.png";
            case 'Fiat':
                return "https://http2.mlstatic.com/quadro-20x30-fiat-logo-novo-okm--D_NQ_NP_964405-MLB25017713195_082016-F.jpg";
            case 'Chevrolet':
                return "https://www.carlogos.org/logo/Chevrolet-logo-2004-1920x1080.jpg";
            case  'Volkswagen':
                return "https://seeklogo.com/images/V/Volkswagen-logo-213294A3AC-seeklogo.com.png";
            default: 
                return "https://img.pngio.com/cartoon-car-png-yellow-color-transparent-background-image-high-png-picture-of-car-900_900.png";
        }
    }

    renderContent = () => {
        const vehicles = this.state.vehicles.map((vehicle) => (
            <Touchable
                key={vehicle}
                delayPressIn={0}
                onPress={() => this.props.navigation.navigate('Map', { id: vehicle.id })}
                style={styles.vehicle}
                foreground={Touchable.Ripple('#999', true)}>
                <View style={styles.subVehicle}>
                    <View style={styles.imageContainer}>
                        <Image style={styles.image} source={{ uri: this.getLogo(vehicle.fabricante) }} />
                    </View>
                    <View style={styles.textInfo}>
                        <Text style={styles.brand} numberOfLines={2}>{vehicle.fabricante}</Text>
                        <Text style={styles.model}>{vehicle.modelo}</Text>
                        <Text style={styles.date}>Placa: {vehicle.placa}</Text>
                    </View>
                </View>
            </Touchable>
        ));

        return vehicles;
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.titleBar}>
                    <Text style={styles.title}>Veículos</Text>
                    <Touchable
                        onPress={async () => this.props.logout()}
                        delayPressIn={0}
                        style={styles.goOut}
                        foreground={Touchable.Ripple('#999', true)}>
                        <Text style={styles.logout}>sair</Text>
                    </Touchable>
                </View>
                {!this.state.loading ?
                    <ScrollView contentContainerStyle={styles.scroll}>
                        {this.renderContent()}
                    </ScrollView>
                    : <ScrollView contentContainerStyle={styles.scrollCenter}>
                        <Image style={styles.gif} source={require('../../loading.gif')} />
                    </ScrollView>}
            </View>
        )
    }
}

const mapStateToProps = state => {
    console.log(state);
    return ({
        token: state.user.token
    });
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(userActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CarList);