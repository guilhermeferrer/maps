import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    scroll: {
        paddingBottom: 5
    },
    scrollCenter: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    gif: {
        width: 50,
        height: 50,
        resizeMode: 'contain'
    },
    titleBar: {
        flexDirection: 'row',
        minHeight: 60,
        justifyContent: 'space-between',
        alignItems: 'center',
        elevation: 4,
        backgroundColor: 'white',
        paddingVertical: 15,
        paddingLeft: 20
    },
    title: {
        fontSize: 16,
        color: 'rgba(0, 0, 0, .8)'
    },
    goOut: {
        width: 40,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 15
    },
    logout: {
        color: 'rgba(0, 0, 0, .8)'
    },
    btn: {
        width: '90%',
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 15,
        borderWidth: 1,
        borderColor: 'rgba(0, 0, 0, .8)',
        borderRadius: 5,
        overflow: 'hidden'
    },
    btnText: {
        textAlign: 'center',        
        color: 'rgba(0, 0, 0, .8)',
        margin: 15
    },
    vehicle: {
        width: '100%',
        padding: 15,
        overflow: 'hidden',
        borderBottomWidth: 1,
        borderColor: 'rgba(0, 0, 0, .1)'
    },
    subVehicle: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    imageContainer: {
        width: 80,
        height: 80
    },
    image: {
        width: '100%',
        height: '100%',
        resizeMode: 'contain'
    },
    textInfo: {
        marginLeft: 10,
        flex: 1
    },
    model: {
        fontSize: 18,
        color: 'rgba(0, 0, 0, .8)'
    },
    brand: {
        fontSize: 16,
        fontWeight: 'bold',
        color: 'black'
    }
});

export default styles;