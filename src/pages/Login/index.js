import React, { Component } from 'react';
import { View, TextInput, Text, Image } from 'react-native';
import Touchable from 'react-native-platform-touchable';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as userActions from '../../actions/user';

import styles from './styles';

import api from '../../services';

class Login extends Component {

    state = {
        user: "",
        password: "",
        loading: false
    }

    login = async () => {
        this.setState({ error: "", loading: true });
        const formData = new FormData();

        formData.append('txtUserName', this.state.user);
        formData.append('txtPasswd', this.state.password);
        formData.append('txtPushToken', 5);

        await api({
            method: 'post',
            url: '/?method=loginRegister',
            data: formData
        }).then((response) => {
            console.log(response);
            if (!response.data.success) {
                if (response.data.error) {
                    this.setState({ error: response.data.error, loading: false });
                } else {    
                    this.setState({ error: response.data.errors[0], loading: false });
                }
            }
            this.setState({ loading: false });
            this.props.login(response.data.token);
        }).catch((error) => {
            console.log(error.response);
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.inputSection}>
                    <Text style={styles.label}>Usuário</Text>
                    <TextInput
                        style={styles.input}
                        autoCapitalize={"none"}
                        onChangeText={(text) => this.setState({ user: text })}
                    />
                </View>
                <View style={styles.inputSection}>
                    <Text style={styles.label}>Senha</Text>
                    <TextInput
                        style={styles.input}
                        secureTextEntry={true}
                        onChangeText={(text) => this.setState({ password: text })}
                    />
                </View>
                <Touchable
                    onPress={async () => this.login()}
                    delayPressIn={0}
                    style={styles.btn}
                    foreground={Touchable.Ripple('#999', true)}>
                    <View style={styles.btnContainer}>
                        {this.state.loading
                            ? <Image style={styles.gif} source={require('../../loading.gif')} />
                            : <Text style={styles.btnText}>Entrar</Text>
                        }
                    </View>
                </Touchable>
                <Text style={styles.error}>{this.state.error}</Text>
            </View>
        )
    }
}

const mapStateToProps = state => {
    console.log(state);
    return ({
        token: state.user.token
    });
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(userActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);