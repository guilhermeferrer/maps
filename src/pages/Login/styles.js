import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({ 
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    inputSection: {
        width: '90%',
        marginBottom: 10,
        justifyContent: 'center'
    },
    label: {
        fontSize: 16,
        color: 'rgba(0, 0, 0, 1)',
        marginVertical: 5
    },
    input: {
        borderBottomWidth: 1,       
        padding: 0,
        paddingBottom: 5,
        fontSize: 18,
        borderColor: 'rgba(0, 0, 0, .4)',
    },
    btn: {
        width: '90%',
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 15,
        borderWidth: 1,
        borderColor: 'rgba(0, 0, 0, .8)',
        borderRadius: 5,
        overflow: 'hidden'
    },
    btnText: {
        textAlign: 'center',        
        color: 'rgba(0, 0, 0, .8)',
        margin: 15
    },
    error: {
        color: 'red',
        width: '90%',
        textAlign: 'center'
    },
    gif: {
        width: 30,
        height: 30,
        margin: 10
    }
});

export default styles;