import React, { Component } from 'react';
import { View, StyleSheet, Image, ScrollView } from 'react-native';
import MapView from 'react-native-maps';
import api from '../../services';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as userActions from '../../actions/user';

class Map extends Component {

  id = this.props.navigation.state.params.id;

  state = {
    loading: true
  }

  componentDidMount() {
    this.loadCoordinates();
  }

  loadCoordinates = async () => {
    this.setState({ error: "", loading: true });
    const formData = new FormData();

    formData.append('txtToken', this.props.token);
    formData.append('txtIdVehicle', this.id);

    await api({
      method: 'post',
      url: '/?method=lastPositions',
      data: formData
    }).then((response) => {
      console.log(response);
      if (response.data.msg === "Token expirado, cancelado ou não encontrado.") {
        return this.props.logout();
      }
      this.setState({ loading: false, position: response.data.aData[0] });
    }).catch((error) => {
      console.log(error.response);
    });
  }

  render() {
    return (
      <View style={styles.container}>
        {!this.state.loading ?
          <MapView
            style={{ flex: 1, width: '100%' }}
            customMapStyle={[
              {
                featureType: "administrative",
                elementType: "geometry",
                stylers: [
                  {
                    visibility: "off"
                  }
                ]
              },
              {
                featureType: "poi",
                stylers: [
                  {
                    visibility: "off"
                  }
                ]
              },
              {
                featureType: "road",
                elementType: "labels.icon",
                stylers: [
                  {
                    visibility: "off"
                  }
                ]
              },
              {
                featureType: "transit",
                stylers: [
                  {
                    visibility: "off"
                  }
                ]
              }
            ]}
            initialRegion={{
              latitude: parseFloat(this.state.position.latitude),
              longitude: parseFloat(this.state.position.longitude),
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
            }} >
            <MapView.Marker
              coordinate={{
                latitude: parseFloat(this.state.position.latitude),
                longitude: parseFloat(this.state.position.longitude),
              }}
              title={"title"}
              description={"description"}>
              <Image
                source={{ uri: "https://icon-library.net/images/mini-cooper-icon/mini-cooper-icon-26.jpg" }}
                style={{ height: 45, width: 45 }} />
            </MapView.Marker>
          </MapView>
          : <ScrollView contentContainerStyle={styles.scrollCenter}>
            <Image style={styles.gif} source={require('../../loading.gif')} />
          </ScrollView>}
      </View>
    );
  }
}

const mapStateToProps = state => {
  console.log(state);
  return ({
      token: state.user.token
  });
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(userActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Map);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  scrollCenter: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  },
  gif: {
    width: 50,
    height: 50,
    resizeMode: 'contain'
  },
});