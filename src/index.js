import React, { Component } from 'react';
import { SafeAreaView } from 'react-native';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';


import Routes from './routes';
import { store, persistor } from './store';

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <PersistGate loading={null} persistor={persistor}>
                    <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
                        <Routes />
                    </SafeAreaView>
                </PersistGate>
            </Provider>
        );
    }
}

export default App;